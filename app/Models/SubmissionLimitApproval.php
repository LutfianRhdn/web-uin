<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubmissionLimitApproval extends Model
{
    use HasFactory;
    use SoftDeletes;

    // Convert field's type data
    protected $casts = [
        'used_date' => 'date',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
