<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SemesterMission extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function semester() {
        return $this->belongsTo(MSemester::class);
    }

    public function parentActivity() {
        return $this->belongsTo(MParentActivity::class, 'parent_activity_id', 'code');
    }
}
