<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfileActivity extends Model
{
    use HasFactory;
    use SoftDeletes;

    // Convert field's type data
    protected $casts = [
        'approved_at' => 'date',
    ];

    public function profileUser() {
        return $this->belongsTo(Profile::class, 'profile_id', 'id');
    }

    public function userApproved() {
        return $this->belongsTo(User::class, 'approved_by', 'id');
    }

    public function typeActivity() {
        return $this->belongsTo(TypeActivity::class);
    }
}
