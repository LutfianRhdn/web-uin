<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeActivity extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function activity() {
        return $this->belongsTo(MActivity::class);
    }

    public function level() {
        return $this->belongsTo(MLevel::class);
    }

    public function roleActivity() {
        return $this->belongsTo(MRoleActivity::class, 'role_activity_id', 'id');
    }

    public function profileActivities()
    {
        return $this->hasMany(TypeActivity::class);
    }
}