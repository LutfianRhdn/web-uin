<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\MParentActivity;
use App\Models\MActivity;
use App\Models\MLevel;
use App\Models\MRoleActivity;

class FormData extends Component
{
    private $pathView;

    public $options;
    public $data;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($pathView, $options, $data = [])
    {
        $this->options = $options;
        $this->pathView = $pathView;
        $this->data = $data;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.' . $this->pathView, 
            [ 'data' => $this->data ]);
    }
}
