<?php

namespace App\View\Components;

use Illuminate\View\Component;

class TableListItem extends Component
{
    public $urlData;
    public $headerColumns;
    public $dataColumns;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($urlData, $headerColumns, $dataColumns)
    {
        $this->urlData = $urlData;
        $this->headerColumns = $headerColumns;
        $this->dataColumns = $dataColumns;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.table-list-item');
    }
}
