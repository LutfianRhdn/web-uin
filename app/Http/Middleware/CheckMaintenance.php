<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckMaintenance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $appDebug = (bool) env('APP_MAINTENANCE');
        if ($appDebug == true) {
            return response()->view('pages.maintenance.index');
        }

        return $next($request);
    }
}
