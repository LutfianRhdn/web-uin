<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DataTables;
use PDF;

use App\Models\ProfileActivity;

class ReportController extends Controller
{
    private $className;

    /**
     * Constructor.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->className = $this->getClassName(get_class($this));
    }

    public function indexDosen(Request $request)
    {
        $titlePage = 'Laporan Persetujuan Dosen';

        // Get params from URL
        $year = isset($request->year) ? $request->year : null;
        $month = isset($request->month) ? $request->month : null;

        return $this->getViewPage($this->pageDefault, 'index', compact('titlePage'), [
                'pathHeader' => 'pages/' . $this->className . '/headerIndex',
                'urlData' => route('report.dataDosen', ['year' => $year, 'month' => $month]),
                'headerColumns' => [
                    ['name' => 'No'],
                    ['name' => 'NIM'],
                    ['name' => 'Nama Mahasiswa'],
                    ['name' => 'Semester'],
                    ['name' => 'Kegiatan'],
                    ['name' => 'Skor'],
                    ['name' => 'Status'],
                    ['name' => 'Tanggal Disetujui']
                ],
                'dataColumns' => [
                    ['data' => 'DT_RowIndex', 'width' => '3%', 'className' => 'text-center'],
                    ['data' => 'profile_user.user.no_id', 'className' => 'text-center'],
                    ['data' => 'profile_user.user.name'],
                    ['data' => 'profile_user.semester.name', 'className' => 'text-center'],
                    ['data' => 'type_activity.activity.name', 'className' => 'text-center'],
                    ['data' => 'total_score', 'className' => 'text-center'],
                    ['data' => 'col_status', 'className' => 'text-center'],
                    ['data' => 'col_approved_at', 'className' => 'text-center'],
                ]]);
    }

    public function dataDosen(Request $request, $year = null, $month = null)
    {
        if (! $request->ajax())
            return;

        // Initial variable
        $year = $year != null ? $year : date('Y');
        
        // Get data by query
        $query = ProfileActivity::query();
        $query->with(['profileUser.user', 'profileUser.semester', 'typeActivity.activity']);
        $query->where('approved_by', Auth::user()->id);
        $query->whereYear('approved_at', $year);

        if ($month != null) $query->whereMonth('approved_at', $month);

        $query->orderBy('created_at', 'DESC');
        $data = $query->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('col_status', function($row) {
                $value = $row->status;
                switch ($value) {
                    case 'WAITING' :
                        return 'Menunggu Konfirmasi';
                        break;
                    case 'VERIFY' :
                        return 'Disetujui';
                        break;
                    case 'REJECT' :
                        return 'Ditolak';
                        break;
                }
            })
            ->addColumn('col_approved_at', function($row) {
                return $row->approved_at->isoFormat('dddd, D MMMM Y');
            })
            ->make(true);
    }

    public function printPDF($year = null, $month = null)
    {
        // Initial variable
        $year = $year != null ? $year : date('Y');

        // Get data by query
        $query = ProfileActivity::query();
        $query->with(['profileUser.user', 'profileUser.semester', 'typeActivity.activity']);
        $query->where('approved_by', Auth::user()->id);
        $query->whereYear('approved_at', $year);

        if ($month != null) $query->whereMonth('approved_at', $month);

        $query->orderBy('created_at', 'DESC');
        $data = $query->get();

        $pdf = PDF::loadview('pages/' . $this->className . '/printPDF', ['year' => $year, 'month' => $month, 'data' => $data]);
    	return $pdf->download('laporan-persetujuan-dosen-' . Auth::user()->name . '-' . date('Ymd-His') . '.pdf');
    }
}