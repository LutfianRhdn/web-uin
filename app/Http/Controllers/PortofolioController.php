<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use DataTables;

use App\Models\Profile;
use App\Models\ProfileActivity;
use App\Models\MSetting;
use App\Models\SubmissionLimitApproval;

class PortofolioController extends Controller
{
    private $className;

    /**
     * Constructor.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->className = $this->getClassName(get_class($this));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($params)
    {
        $titlePage = 'Verifikasi Kegiatan';

        return $this->getViewPage($this->className, 'index', compact('titlePage'), [
                'urlData' => route('portofolio.dataIndex', $params),
                'headerColumns' => [
                    ['name' => 'No'],
                    ['name' => 'NIM'],
                    ['name' => 'Nama Mahasiswa'],
                    ['name' => 'Status'],
                    ['name' => 'Tanggal Masuk'],
                    ['name' => 'Aksi'],
                ],
                'dataColumns' => [
                    ['data' => 'DT_RowIndex', 'width' => '3%', 'className' => 'text-center'],
                    ['data' => 'profile_user.user.no_id', 'className' => 'text-center'],
                    ['data' => 'profile_user.user.name'],
                    ['data' => 'status', 'className' => 'text-center'],
                    ['data' => 'impl_date', 'className' => 'text-center'],
                    ['data' => 'action', 'orderable' => false, 'searchable' => false, 'className' => 'text-center'],
                ]
            ]);
    }

    public function indexVerify()
    {
        $titlePage = 'Mahasiswa Terverifikasi';

        return $this->getViewPage($this->className, 'index', compact('titlePage'), [
                'urlData' => route('portofolio.dataIndexVerify'),
                'headerColumns' => [
                    ['name' => 'No'],
                    ['name' => 'NIM'],
                    ['name' => 'Nama Mahasiswa'],
                    ['name' => 'Skor'],
                    ['name' => 'Status'],
                    ['name' => 'Aksi'],
                ],
                'dataColumns' => [
                    ['data' => 'DT_RowIndex', 'width' => '3%', 'className' => 'text-center'],
                    ['data' => 'profile_user.user.no_id', 'className' => 'text-center'],
                    ['data' => 'profile_user.user.name'],
                    ['data' => 'total_score', 'className' => 'text-center'],
                    ['data' => 'status', 'className' => 'text-center'],
                    ['data' => 'action', 'orderable' => false, 'searchable' => false, 'className' => 'text-center'],
                ]
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($profile_activity_id)
    {
        $data = ProfileActivity::
            with(['profileUser.user.role'])
            ->find($profile_activity_id);

        return $this->getViewPage($this->className, 'show', [
            'titlePage' => 'Detail Verifikasi'],
            compact('data'));
    }

    public function showDetail($user_id)
    {
        return app('App\Http\Controllers\CollegerController')->show($user_id);
    }

    public function dataIndex(Request $request, $type)
    {
        if ($request->ajax()) {
            $data = ProfileActivity::
                with('profileUser.user')
                ->where('status', strtoupper($type))
                ->orderBy('created_at', 'DESC')
                ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('impl_date', function($row) {
                    return formatDate($row->implementation_date);
                })
                ->addColumn('action', function($row) {
                    $action = '
                        <a href="javascript:;" class="edit btn btn-info btn-sm" data-toggle="modal" data-target="#modalSendMessage" data-user="' . $row->profileUser->user_id . '" data-name="' . $row->profileUser->user->name . '">Kirim Pesan</a>
                        <a href="' . route('portofolio.show', $row->id) . '" class="edit btn btn-warning btn-sm">Detail</a>';
                    return $action;
                })
                ->editColumn('status', function($row) {
                    $value = '-';
                    switch ($row->status) {
                        case 'WAITING':
                            $value = 'Menunggu Verifikasi';
                            break;
                        case 'VERIFY':
                            $value = 'Terverifikasi';
                            break;
                        case 'REJECT':
                            $value = 'Ditolak';
                            break;
                    }
                    return $value;
                })
                ->make(true);
        }
    }

    public function dataIndexVerify(Request $request)
    {
        if ($request->ajax()) {
            $data = ProfileActivity::
                with('profileUser.user')
                ->where('status', 'VERIFY')
                ->orderBy('created_at', 'DESC')
                ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row) {
                    $action = '
                        <a href="' . route('portofolio.showDetail', $row->profileUser->user_id) . '" class="edit btn btn-warning btn-sm">Detail</a>';
                    return $action;
                })
                ->editColumn('status', function($row) {
                    return 'Terverifikasi';
                })
                ->make(true);
        }
    }

    private function _updateStatus($profile_activity_id, $status)
    {
        // Init variable
        $userIdLogin = Auth::user()->id;

        // Get limit setting approval
        $getSetting = MSetting::first();
        $limitApprove = isset($getSetting->limit_approval) ? ((int) $getSetting->limit_approval) : 0;

        // Get profile activity approved by Dosen
        $countDataApproved = ProfileActivity::
            where('approved_by', $userIdLogin)
            ->whereDate('approved_at', Carbon::today())
            ->count();

        if ($limitApprove > 0 && ($countDataApproved >= $limitApprove)) {

            // Get data and summary limit approval lecturer
            // Where submission was approved
            $countSubmission = SubmissionLimitApproval::
                where('user_id', Auth::user()->id)
                ->where('status_approve', 1)
                ->whereDate('created_at', Carbon::today())
                ->get()
                ->sum('limit_approval');

            // Get the rest of approval lecturer
            $restApprove = $countSubmission - ($countDataApproved - $limitApprove);
            if ($restApprove == 0)
                return redirect()->route('portofolio.index', 'waiting')->with('error', 'Tidak bisa melakukan persetujuan. Batas persetujuan telah terpenuhi!');
        }

        // Get profile activity by profile_activity_id
        $data = ProfileActivity::
            with('typeActivity')
            ->find($profile_activity_id);

        // Get point
        $pointTypeAct = $data->typeActivity->point;

        // Update status & total_score in ProfileActivity
        $data->status = $status;
        $data->total_score = $pointTypeAct;
        $data->approved_by = $userIdLogin;
        $data->approved_at = date('Y-m-d H:i:s');

        // Get profile user by profile_id in ProfileActivity
        $profile = Profile::find($data->profile_id);
        if ($status == 'VERIFY') {
            // Update profile score in Profile
            $profile->profile_score = ($profile->profile_score + $pointTypeAct);
            $profile->save();
        }
        else if ($status == 'REJECT')
            $data->rejected_date = date('Y-m-d H:i:s');

        $response = $data->save();

        if ($response && $status == 'REJECT') {
            sendNotification([
                'user_id' => $profile->user_id,
                'title' => 'Pengajuan Portofolio Ditolak!',
                'message' => 'Pengajuan Portofolio anda ditolak oleh Dosen.',
                'url_direct' => route('dashboard.index')
            ]);
        }

        if (! $response) 
            return false;

        return true;
    }

    public function storeApproveData($profile_activity_id)
    {
        $this->_updateStatus($profile_activity_id, 'VERIFY');

        return redirect()->route('portofolio.index', 'waiting');
    }

    public function storeRejectData($profile_activity_id)
    {
        $response = $this->_updateStatus($profile_activity_id, 'REJECT');

        return redirect()->route('portofolio.index', 'waiting');
    }

    public function storeSendMessage(Request $request)
    {
        $validatedData = $request->validate([
            'user' => 'required',
            'message' => 'required'
        ]);

        $user = $request->user;
        $message = $request->message;

        sendNotification([
            'user_id' => $user,
            'title' => 'Pesan Masuk ' . (isset(Auth::user()->name) ? "dari " . Auth::user()->name : null),
            'message' => $message,
            'url_direct' => route('dashboard.index')
        ]);

        return redirect()->back();
    }
}