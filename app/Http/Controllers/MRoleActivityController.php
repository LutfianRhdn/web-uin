<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;

use App\Models\MRoleActivity;

class MRoleActivityController extends Controller
{
    private $className;

    /**
     * Constructor.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->className = $this->getClassName(get_class($this));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'Daftar Master Peran Aktivitas';

        return $this->getViewPage($this->pageDefault, 'index', compact('titlePage'), [
                'pathHeader' => 'pages/' . $this->className . '/headerIndex',
                'urlData' => route('mRoleActivity.dataIndex'),
                'headerColumns' => [
                    ['name' => 'No'],
                    ['name' => 'Nama Tingkat'],
                    ['name' => 'Aksi'],
                ],
                'dataColumns' => [
                    ['data' => 'DT_RowIndex', 'width' => '3%', 'className' => 'text-center'],
                    ['data' => 'name'],
                    ['data' => 'action', 'orderable' => false, 'searchable' => false, 'className' => 'text-center'],
                ]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->getViewPage($this->pageDefault, 'formData', [
            'titlePage' => 'Tambah Master Peran Aktivitas',
            'urlComponent' => 'formData.mRoleActivity',
            'formUrl' => route('mRoleActivity.store'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required'
        ]);

        $data = new MRoleActivity;
        $data->name = $request->name;
        $data->save();

        return redirect()->route('mRoleActivity.index')->with('success', 'Berhasil Menyimpan Data!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MRoleActivity::find($id);

        return $this->getViewPage($this->pageDefault, 'formData', [
            'titlePage' => 'Ubah Master Peran Aktivitas',
            'urlComponent' => 'formData.mRoleActivity',
            'formUrl' => route('mRoleActivity.update', $id),
            'formMethod' => 'PUT',
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required'
        ]);

        $data = MRoleActivity::find($id);
        $data->name = $request->name;
        $data->save();

        return redirect()->route('mRoleActivity.index')->with('success', 'Berhasil Menyimpan Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = MRoleActivity::destroy($id);

        return redirect()->back()->with('success', 'Berhasil Menghapus Data!');
    }

    public function dataIndex(Request $request)
    {
        if (! $request->ajax())
            return;

        $data = MRoleActivity::
            orderBy('name', 'ASC')
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $action = 
                    '<a href="' . route('mRoleActivity.edit', $row->id) . '" class="btn btn-warning btn-sm">Ubah</a>' .
                    '<button type="button" data-text-confirm="Yakin ingin menghapus data ini?" data-method="DELETE" data-url="' . route('mRoleActivity.destroy', $row->id) . '" class="btn-row-submit btn btn-danger btn-sm ml-2">Hapus</button>';
                return $action;
            })
            ->make(true);
    }
}
