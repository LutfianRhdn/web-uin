<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;

use App\Models\MParentActivity;
use App\Models\MActivity;
use App\Models\MLevel;
use App\Models\MRoleActivity;
use App\Models\TypeActivity;

class TypeActivityController extends Controller
{
    private $className;

    /**
     * Constructor.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->className = $this->getClassName(get_class($this));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'Daftar Pembobotan Kegiatan Mahasiswa';

        return $this->getViewPage($this->pageDefault, 'index', compact('titlePage'), [
                'pathHeader' => 'pages/' . $this->className . '/headerIndex',
                'urlData' => route('typeActivity.dataIndex'),
                'headerColumns' => [
                    ['name' => 'No'],
                    ['name' => 'Tipe Kegiatan'],
                    ['name' => 'Nama Kegiatan'],
                    ['name' => 'Peringkat'],
                    ['name' => 'Peran'],
                    ['name' => 'Poin'],
                    ['name' => 'Aksi'],
                ],
                'dataColumns' => [
                    ['data' => 'DT_RowIndex', 'width' => '3%', 'className' => 'text-center'],
                    ['data' => 'activity.parent_activity.name'],
                    ['data' => 'activity.name'],
                    ['data' => 'col_level', 'className' => 'text-center'],
                    ['data' => 'col_role_activity', 'className' => 'text-center'],
                    ['data' => 'point', 'className' => 'text-center'],
                    ['data' => 'action', 'orderable' => false, 'searchable' => false, 'className' => 'text-center'],
                ]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $optParentAct = MParentActivity::orderBy('name', 'ASC')
            ->pluck('name', 'code')
            ->filter();

        $optTypeAct = MActivity::orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->filter();

        $optLevel = MLevel::orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->filter();

        $optRoleAct = MRoleActivity::orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->filter();

        return $this->getViewPage($this->pageDefault, 'formData', [
            'titlePage' => 'Buat Pembobotan Kegiatan Mahasiswa',
            'urlComponent' => 'formData.typeActivity',
            'formUrl' => route('typeActivity.store'),
            'options' => [
                'optParentAct' => $optParentAct,
                'optTypeAct' => $optTypeAct,
                'optLevel' => $optLevel,
                'optRoleAct' => $optRoleAct,
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'parentActivity' => 'required',
            'typeActivity' => 'required',
            'point' => 'required'
        ]);

        // Check data if exist
        if (! $this->onValidationData($request))
            return redirect()->back()->with('error', 'Data yang dimasukkan sudah ada!');

        $data = new TypeActivity;
        $data->activity_id = $request->typeActivity;
        $data->level_id = $request->level;
        $data->role_activity_id = $request->role;
        $data->point = $request->point;
        $data->save();

        return redirect()->route('typeActivity.index')->with('success', 'Berhasil Menyimpan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = TypeActivity::with('activity')->find($id);

        $optParentAct = MParentActivity::orderBy('name', 'ASC')
            ->pluck('name', 'code')
            ->filter();

        $optTypeAct = MActivity::orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->filter();

        $optLevel = MLevel::orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->filter();

        $optRoleAct = MRoleActivity::orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->filter();

        return $this->getViewPage($this->pageDefault, 'formData', [
            'titlePage' => 'Ubah Pembobotan Kegiatan Mahasiswa',
            'urlComponent' => 'formData.typeActivity',
            'formUrl' => route('typeActivity.update', $id),
            'formMethod' => 'PUT',
            'options' => [
                'optParentAct' => $optParentAct,
                'optTypeAct' => $optTypeAct,
                'optLevel' => $optLevel,
                'optRoleAct' => $optRoleAct,
            ],
            'data' => $data
        ]);
    }

    private function onValidationData(Request $request)
    {
        $checkData = TypeActivity::
            where('activity_id', $request->typeActivity)
            ->where('level_id', $request->level)
            ->where('role_activity_id', $request->role)
            ->first();

        if ($checkData != null)
            return false;

        return true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validatedData = $request->validate([
            'parentActivity' => 'required',
            'typeActivity' => 'required',
            'point' => 'required'
        ]);

        $data = TypeActivity::find($id);

        if (
            $data->activity_id != $request->typeActivity ||
            $data->level_id != $request->level ||
            $data->role_activity_id != $request->role
        ) {
            // Check data if exist
            if (! $this->onValidationData($request))
                return redirect()->back()->with('error', 'Data yang dimasukkan sudah ada!');
        }

        $data->activity_id = $request->typeActivity;
        $data->level_id = $request->level;
        $data->role_activity_id = $request->role;
        $data->point = $request->point;
        $data->save();

        return redirect()->route('typeActivity.index')->with('success', 'Berhasil Menyimpan Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $data = TypeActivity::destroy($id);

        return redirect()->back()->with('success', 'Berhasil Menghapus Data!');
    }

    public function dataIndex(Request $request)
    {
        if (! $request->ajax())
            return;

        $data = TypeActivity::
            with(['activity.parentActivity', 'level', 'roleActivity'])
            ->orderBy('created_at', 'DESC')
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('col_level', function($row) {
                $value = $row->level;
                if ($value == null)
                    return '-';

                return $value->name;
            })
            ->addColumn('col_role_activity', function($row) {
                $value = $row->roleActivity;
                if ($value == null)
                    return '-';

                return $value->name;
            })
            ->addColumn('action', function($row) {
                $action = 
                    '<a href="' . route('typeActivity.edit', $row->id) . '" class="btn btn-warning btn-sm">Ubah</a>' .
                    '<button type="button" data-text-confirm="Yakin ingin menghapus data ini?" data-method="DELETE" data-url="' . route('typeActivity.destroy', $row->id) . '" class="btn-row-submit btn btn-danger btn-sm ml-2">Hapus</button>';
                return $action;
            })
            ->make(true);
    }

    public function dataOptKegiatan(Request $request) 
    {
        if (! $request->ajax())
            return;

        $codeAct = $request->code;
        $data = MActivity::where('code', $codeAct)
            ->pluck('name', 'id')
            ->filter();

        return response()->json($data);
    }
}