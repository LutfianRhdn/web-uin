<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DataTables;

use App\Models\Profile;
use App\Models\MParentActivity;

class LeaderboardController extends Controller
{
    private $className;

    /**
     * Constructor.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->className = $this->getClassName(get_class($this));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Initial Variable
        $titlePage = 'Daftar Leaderboard';
        $colorBg = ['danger', 'primary', 'info', 'success', 'warning', 'secondary', 'default', 'success', 'info', 'warning'];
        $semesterIdLogin = null;

        // Get value request
        $typeActivity = isset($request->typeActivity) ? $request->typeActivity : null;
        $degree = $request->degree;
        if ($degree == 'ONE_DEGREE') {
            $semesterIdLogin = isset(Auth::user()->profile) ? Auth::user()->profile->semester_id : null;
        }

        // Get data parent activity
        $optActivity = MParentActivity::orderBy('name', 'ASC')
            ->pluck('name', 'code');

        $query = Profile::query();
        $query->with(['profileActivities.typeActivity.activity', 'user.role']);
        $query->where('profile_score', '>=', '10');
        $query->whereHas('user.role', function($query) {
            $query->where('code', 'MHS');
        });

        if ($semesterIdLogin != null)
            $query->where('semester_id', $semesterIdLogin);

        if ($typeActivity != null)
            $query->whereHas('profileActivities.typeActivity.activity', function($query) use ($typeActivity) {
                $query->where('code', $typeActivity);
            });

        $query->orderBy('profile_score', 'DESC');
        $query->skip(0)->take(10);

        $result = $query->get();

        return $this->getViewPage($this->className, 'index', compact('titlePage', 'optActivity'), [
            'data' => $result,
            'colorBg' => $colorBg,
            'semester' => ($semesterIdLogin != null ? 'ONE_DEGREE' : 'ALL_DEGREE'),
            'typeActivity' => $typeActivity
        ]);
    }
}
