<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use Auth;

use App\Models\MActivity;
use App\Models\MLevel;
use App\Models\MRoleActivity;
use App\Models\Profile;
use App\Models\TypeActivity;
use App\Models\ProfileActivity;

class ActivityController extends Controller
{
    private $className;

    /**
     * Constructor.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->className = $this->getClassName(get_class($this));
    }

    public function indexMenu()
    {
        return $this->getViewPage($this->className, 'menu', [
                'titlePage' => 'Kegiatan Mahasiswa'
            ]);
    }

    private function getCodeActivity($type)
    {
        $result = null;
        switch (strtoupper($type)) {
            case "KEWIRAUSAHAAN" :
                $result = 'KEWIRAUSAHAAN';
                break;
            case "MINAT-BAKAT" :
                $result = 'MINAT_BAKAT';
                break;
            case 'KEPEMIMPINAN' :
                $result = 'ORGANISASI_KEPEMIMPINAN';
                break;
            case 'KEILMUAN' :
                $result = 'PENALARAN_KEILMUAN';
                break;
            case 'PENGABDIAN' :
                $result = 'PENGABDIAN_MASYARAKAT';
                break;
        }
        
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type)
    {
        $titlePage = null;
        $panelColor = 'secondary';

        switch (strtoupper($type)) {
            case "KEILMUAN":
                $titlePage = 'Kegiatan Penalaran & Keilmuan';
                $panelColor = 'info';
                break;
            case "KEPEMIMPINAN":
                $titlePage = 'Organisasi & Kepemimpinan';
                $panelColor = 'success';
                break;
            case "MINAT-BAKAT":
                $titlePage = 'Kegiatan Minat & Bakat';
                $panelColor = 'warning';
                break;
            case "PENGABDIAN":
                $titlePage = 'Pengabdian Pada Masyarakat';
                $panelColor = 'danger';
                break;
            case "KEWIRAUSAHAAN":
                $titlePage = 'Kegiatan Kewirausahaan';
                $panelColor = 'primary';
                break;
            default:
                abort(404);
                break;
        }

        $codeActivity = $this->getCodeActivity($type);
        $parentActClause = function($query) use ($codeActivity) {
            $query->where('code', $codeActivity);
        };

        $optTypeAct = MActivity::
            with('parentActivity')
            ->whereHas('parentActivity', $parentActClause)
            ->orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->filter();

        return $this->getViewPage($this->className, 'create',
            [
                'titlePage' => $titlePage,
                'panelColor' => $panelColor,
                'options' => [
                    'optTypeAct' => $optTypeAct,
                ],
                'codeActivity' => $codeActivity
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'typeActivity' => 'required',
            'name_activity' => 'required',
            'implementation_date' => 'required',
            'file_document' => 'required|mimes:jpeg,png,jpg,svg,pdf,xlsx,xls,docx,doc',
        ]);

        $getTypeAct = TypeActivity::
            where('activity_id', $request->typeActivity)
            ->where('level_id', $request->level)
            ->where('role_activity_id', $request->role)
            ->first();

        if ($getTypeAct == null) {
            return redirect()->back()->with('error', 'Tidak bisa mengajukan kegiatan. Kategori Kegiatan & Tingkat & Peran tidak sesuai!');
        }

        DB::transaction(function() use ($request, $getTypeAct) {
            $fileName = time() . '-' . Str::random(40) . '.' . $request->file_document->extension();

            $data = new ProfileActivity;
            $data->profile_id = Auth::user()->profile->id;
            $data->type_activity_id = $getTypeAct->id;
            $data->file_document = $fileName;
            $data->name = $request->name_activity;
            $data->implementation_date = $request->implementation_date;
            $data->save();

            if ($data)
                $request->file_document->move(public_path('uploads/file_document'), $fileName);
        });

        return redirect()->back()->with('success', 'Berhasil Menyimpan Data!');
    }

    public function getOptLevel(Request $request)
    {
        if (! $request->ajax())
            return;

        $typeActivity = $request->typeActivity;

        $data = TypeActivity::
            with('level')
            ->where('activity_id', $typeActivity)
            ->get()
            ->pluck('level.name', 'level_id')
            ->filter();

        return response()->json($data);
    }

    public function getOptRoleAct(Request $request)
    {
        if (! $request->ajax())
            return;

        $typeActivity = $request->typeActivity;
        $level = $request->level;

        $data = TypeActivity::
            with(['level', 'roleActivity'])
            ->where('activity_id', $typeActivity)
            ->where('level_id', $level)
            ->get()
            ->pluck('roleActivity.name', 'role_activity_id')
            ->filter();

        return response()->json($data);
    }
}
