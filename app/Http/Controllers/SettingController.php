<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\MSetting;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = MSetting::first();

        return $this->getViewPage($this->pageDefault, 'formData', [
            'titlePage' => 'Pengaturan',
            'urlComponent' => 'formData.setting',
            'formUrl' => route('setting.store'),
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = MSetting::all();
        if (count($data) > 0) {
            $update = $data->first();
            $update->limit_approval = $request->limitApproval;
            $update->save();
        } else {
            $insert = new MSetting;
            $insert->limit_approval = $request->limitApproval;
            $insert->save();
        }

        return redirect()->back()->with('success', 'Berhasil Merubah Pengaturan Aplikasi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
