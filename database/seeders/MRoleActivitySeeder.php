<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MRoleActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableName = 'm_role_activities';
        $dateNow = date("Y-m-d H:i:s");

        $arrData = [
            [ 'name' => 'Pemateri', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Moderator', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Peserta', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Penulis Pertama', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Penulis Kedua', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Penulis Ketiga', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Presenter', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Individu', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Tim', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Juara 1', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Juara 2', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Juara 3', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Nominasi Lain (Harapan, Terfavorit, dll)', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Ketua', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Pimpinan (Sekum, Bendum & Kabid)', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Staff Bidang/Anggota', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Staff Bidang', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Founder', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Pengurus', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Anggota', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'SC', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'OC', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Struktur Inti (BPH & KADIV)', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Ketua Angkatan', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Sekretaris & Bendahara Angkatan', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Kosma', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Mentor', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Mentee', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Pelatih/Pemateri', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Penyelenggara', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Inisiator', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
        ];

        foreach ($arrData as $row) {
            DB::table($tableName)->insert($row);
        }
    }
}
