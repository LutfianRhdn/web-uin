<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MSemesterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableName = 'm_semesters';
        $dateNow = date("Y-m-d H:i:s");

        $arrData = [
            [ 'name' => 'Semester 1', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Semester 2', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Semester 3', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Semester 4', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Semester 5', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Semester 6', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Semester 7', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Semester 8', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
        ];

        foreach ($arrData as $row) {
            DB::table($tableName)->insert($row);
        }
    }
}
