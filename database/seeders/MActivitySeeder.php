<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableName = 'm_activities';
        $dateNow = date("Y-m-d H:i:s");

        $codePK = 'PENALARAN_KEILMUAN';
        $codeOK = 'ORGANISASI_KEPEMIMPINAN';
        $codeMB = 'MINAT_BAKAT';
        $codePM = 'PENGABDIAN_MASYARAKAT';
        $codeKW = 'KEWIRAUSAHAAN';

        $arrData = [
            [ 'code' => $codePK, 'name' => 'Seminar atau Diskusi Ilmiah', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePK, 'name' => 'Workshop/Seminar Wirausaha', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePK, 'name' => 'Menulis Karya Tulis Ilmiah', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePK, 'name' => 'Mempublikasikan Karya Tulis Ilmiah', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePK, 'name' => 'Mengikuti Prosiding Ilmiah / Conferences', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePK, 'name' => 'Menjadi Reviewer', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePK, 'name' => 'Mengikuti Perlombaan Ilmiah', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePK, 'name' => 'Memenangkan Perlombaan Ilmiah', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePK, 'name' => 'Asisten Dosen / Asisten Laboratorium', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePK, 'name' => 'Mengikuti Kuliah Dosen Tamu', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePK, 'name' => 'Terlibat dalam penelitian pihak lain (menjadi asisten/kontributor penelitian)', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePK, 'name' => 'Pertukaran Pelajar', 'created_at' => $dateNow, 'updated_at' => $dateNow ],

            [ 'code' => $codeOK, 'name' => 'Keterlibatan dalam Organisasi Intra Kampus', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeOK, 'name' => 'Keterlibatan Organisasi Eksternal Kampus/Umum', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeOK, 'name' => 'Komunitas/UKM/Pengembangan Diri', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeOK, 'name' => 'Kepanitiaan Kegiatan Kemahasiswaan', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeOK, 'name' => 'Kepanitian Kegiatan Eksternal Kampus', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeOK, 'name' => 'Pelatihan/Seminar Keorganisasian atau Kepemimpinan', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeOK, 'name' => 'Pengurus Angkatan', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeOK, 'name' => 'Terlibat dalam kegiatan Mentorship', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeOK, 'name' => 'Mencalonkan diri menjadi Ketua Organisasi/Komunitas', 'created_at' => $dateNow, 'updated_at' => $dateNow ],

            [ 'code' => $codeMB, 'name' => 'Terlibat dalam pementasan non kompetisi (Pentas Seni, teater, kajian, dll)', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeMB, 'name' => 'Terlibat dalam kegiatan pelatihan vokasi (non psikologi)', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeMB, 'name' => 'Mengikuti Kompetisi Minat Bakat (olahraga, seni, foto, video, sastra, dll)', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeMB, 'name' => 'Memenangkan Kompetisi Minat Bakat (olahraga, seni, foto, video, sastra, dll)', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeMB, 'name' => 'Menjadi Duta (Kampus, daerah, Sosial, budaya, bahasa, gerakan, dsb)', 'created_at' => $dateNow, 'updated_at' => $dateNow ],

            [ 'code' => $codePM, 'name' => 'Menjadi volunteer dalam kegiatan tanggap bencana & sosial lainnya', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePM, 'name' => 'Mengikuti kegiatan bakti sosial', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePM, 'name' => 'Ikut serta dalam kampanye sosial', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePM, 'name' => 'Pelatihan/ workshop/diskusi sosial atau kebencanaan', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePM, 'name' => 'Project sosial lainnya yang dikerjakan individual atau kelompok (Intervensi sosial, konseling dll)', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePM, 'name' => 'Pengajar Sukarela', 'created_at' => $dateNow, 'updated_at' => $dateNow ],

            [ 'code' => $codeKW, 'name' => 'Pelatihan/Workshop/Seminar Kewirausahaan', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeKW, 'name' => 'Mengelola Kewirausahaan', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeKW, 'name' => 'Ikut serta dalam Bazar', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
        ];

        foreach ($arrData as $row) {
            DB::table($tableName)->insert($row);
        }
    }
}
