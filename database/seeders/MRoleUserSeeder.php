<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MRoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableName = 'm_role_users';
        $dateNow = date("Y-m-d H:i:s");

        $arrData = [
            [ 'code' => 'SU', 'name' => 'Super Admin', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => 'DSN', 'name' => 'Dosen', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => 'MHS', 'name' => 'Mahasiswa', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
        ];

        foreach ($arrData as $row) {
            DB::table($tableName)->insert($row);
        }
    }
}
