<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableName = 'm_settings';
        $dateNow = date("Y-m-d H:i:s");

        DB::table($tableName)->insert([
            'limit_approval' => 10,
            'created_at' => $dateNow, 
            'updated_at' => $dateNow 
        ]);
    }
}
