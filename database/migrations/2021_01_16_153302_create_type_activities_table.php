<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypeActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_activities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('activity_id');
            $table->unsignedBigInteger('level_id')->nullable();
            $table->unsignedBigInteger('role_activity_id')->nullable();
            $table->double('point', 8, 2)->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('activity_id')->references('id')->on('m_activities')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('level_id')->references('id')->on('m_levels')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('role_activity_id')->references('id')->on('m_role_activities')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('type_activities');
    }
}
