<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ActivityController;
use App\Http\Controllers\PortofolioController;
use App\Http\Controllers\CollegerController;
use App\Http\Controllers\TypeActivityController;
use App\Http\Controllers\LeaderboardController;
use App\Http\Controllers\SemesterMissionController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SubmissionLimitController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\MActivityController;
use App\Http\Controllers\MLevelController;
use App\Http\Controllers\MRoleActivityController;
use App\Http\Controllers\CompanyProfileController;

Route::middleware(['checkMaintenance'])->group(function () {
    Route::get('/', [CompanyProfileController::class, 'index']);
    Route::middleware(['guest'])->group(function () {
        Route::get('login', [UserController::class, 'indexLogin'])->name('auth.indexLogin');
        Route::post('login', [UserController::class, 'storeLogin'])->name('auth.storeLogin');
        Route::get('register', [UserController::class, 'indexRegister'])->name('auth.indexRegister');
        Route::post('register', [UserController::class, 'storeRegister'])->name('auth.storeRegister');
    });

    Route::middleware(['auth'])->group(function () {

        // Ajax Request
        Route::post('fetch-data/option-level', [ActivityController::class, 'getOptLevel'])->name('ajax.getOptLevel');
        Route::post('fetch-data/option-role-activity', [ActivityController::class, 'getOptRoleAct'])->name('ajax.getOptRoleAct');

        // Logout Session
        Route::get('logout', [UserController::class, 'storeLogout'])->name('auth.storeLogout');

        // Profile
        Route::get('/profile/started', [ProfileController::class, 'indexStarted'])->name('profile.indexStarted');
        Route::post('/profile/started', [ProfileController::class, 'storeStarted'])->name('profile.storeStarted');

        Route::get('/profile/setting', [ProfileController::class, 'indexSetting'])->name('profile.indexSetting');
        Route::put('/profile/setting', [ProfileController::class, 'storeSetting'])->name('profile.storeSetting');

        // Dashboard
        // Route::get('/', [DashboardController::class, 'index'])->name('dashboard.index');
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard.index');
        Route::get('/fetch-data/data-prestasi', [DashboardController::class, 'dataPrestasi'])->name('dashboard.dataPrestasi');

        // Portofolio
        Route::get('colleger/data', [CollegerController::class, 'dataIndex'])->name('colleger.dataIndex');
        Route::get('colleger/data-portofolio/{id}', [CollegerController::class, 'dataPortofolio'])->name('colleger.dataPortofolio');
        Route::get('colleger/print-pdf/{user_id}', [CollegerController::class, 'printPDF'])->name('colleger.printPDF');
        Route::resource('colleger', CollegerController::class);

        // Activity
        Route::get('activity/menu', [ActivityController::class, 'indexMenu'])->name('activity.indexMenu');
        Route::get('activity/{type}/create', [ActivityController::class, 'create'])->name('activity.create');
        Route::resource('activity', ActivityController::class, ['except' => ['create']]);

        // Portofolio
        Route::get('portofolio', function () {
            return redirect()->route('portofolio.index', 'waiting');
        });
        Route::get('portofolio/data/{id}', [PortofolioController::class, 'dataIndex'])->name('portofolio.dataIndex');
        Route::get('portofolio/data-verify', [PortofolioController::class, 'dataIndexVerify'])->name('portofolio.dataIndexVerify');

        Route::post('portofolio/update-verify/{id}', [PortofolioController::class, 'storeApproveData'])->name('portofolio.postApproveData');
        Route::post('portofolio/update-reject/{id}', [PortofolioController::class, 'storeRejectData'])->name('portofolio.postRejectData');
        Route::post('portofolio/send-message', [PortofolioController::class, 'storeSendMessage'])->name('portofolio.sendMessage');

        Route::get('portofolio/verify', [PortofolioController::class, 'indexVerify'])->name('portofolio.indexVerify');
        Route::get('portofolio/show/{id}', [PortofolioController::class, 'show'])->name('portofolio.show');
        Route::get('portofolio/show-detail/{id}', [PortofolioController::class, 'showDetail'])->name('portofolio.showDetail');
        Route::get('portofolio/{id}', [PortofolioController::class, 'index'])->name('portofolio.index');
        Route::resource('portofolio', ActivityController::class, ['except' => ['index', 'show']]);

        // Leaderboard
        Route::resource('leaderboard', LeaderboardController::class, ['names' => 'leaderboard']);
        Route::post('leaderboard', [LeaderboardController::class, 'index'])->name('leaderboard.postIndex');

        // Master Data
        Route::prefix('master')->group(function () {

            // User
            Route::get('user/data', [UserController::class, 'dataIndex'])->name('user.dataIndex');
            Route::post('user/reset-password/{id}', [UserController::class, 'storeResetPassword'])->name('user.resetPassword');
            Route::post('user/change-password', [UserController::class, 'storeChangePassword'])->name('user.changePassword');
            Route::resource('user', UserController::class);

            // Activity
            Route::get('activity/data', [MActivityController::class, 'dataIndex'])->name('mActivity.dataIndex');
            Route::resource('activity', MActivityController::class, ['names' => 'mActivity']);

            // Level
            Route::get('level/data', [MLevelController::class, 'dataIndex'])->name('mLevel.dataIndex');
            Route::resource('level', MLevelController::class, ['names' => 'mLevel']);

            // Role Activity
            Route::get('role-activity/data', [MRoleActivityController::class, 'dataIndex'])->name('mRoleActivity.dataIndex');
            Route::resource('role-activity', MRoleActivityController::class, ['names' => 'mRoleActivity']);

            // Type Activity
            Route::get('type-activity/data', [TypeActivityController::class, 'dataIndex'])->name('typeActivity.dataIndex');
            Route::post('type-activity/data-opt-kegiatan', [TypeActivityController::class, 'dataOptKegiatan'])->name('typeActivity.dataOptKegiatan');
            Route::resource('type-activity', TypeActivityController::class, ['names' => 'typeActivity']);

            // Semester Mission
            Route::get('semester-mission/data', [SemesterMissionController::class, 'dataIndex'])->name('semesterMission.dataIndex');
            Route::resource('semester-mission', SemesterMissionController::class, ['names' => 'semesterMission']);
        });

        // Notification
        Route::get('notification/read', [NotificationController::class, 'redirectData'])->name('notification.redirectData');
        Route::post('notification', [NotificationController::class, 'getData'])->name('notification.getData');

        // Submission Limit
        Route::get('submission-limit/review', [SubmissionLimitController::class, 'indexReview'])->name('submissionLimit.indexReview');

        Route::get('submission-limit/data', [SubmissionLimitController::class, 'dataIndex'])->name('submissionLimit.dataIndex');
        Route::get('submission-limit/data-review', [SubmissionLimitController::class, 'dataReview'])->name('submissionLimit.dataReview');

        Route::post('submission-limit/update-verify/{id}', [SubmissionLimitController::class, 'storeApproveData'])->name('submissionLimit.storeApproveData');
        Route::post('submission-limit/update-reject/{id}', [SubmissionLimitController::class, 'storeRejectData'])->name('submissionLimit.storeRejectData');

        Route::resource('submission-limit', SubmissionLimitController::class, ['names' => 'submissionLimit']);

        // Report
        Route::get('report/dosen', [ReportController::class, 'indexDosen'])->name('report.indexDosen');
        Route::get('report/data-dosen/{year?}/{month?}', [ReportController::class, 'dataDosen'])->name('report.dataDosen');

        Route::get('report/print-pdf/{year?}/{month?}', [ReportController::class, 'printPDF'])->name('report.printPDF');

        // Setting
        Route::resource('setting', SettingController::class);
    });
});
