<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label>Jenis Kegiatan</label>
            <select id="parentActivity" name="parentActivity" class="form-control select2" required>
                @foreach ($options['optParentAct'] as $index => $row)
                    <option value="{{ $index }}" 
                        {{ isset($data['activity']['code']) && $data['activity']['code'] == $index ? 'selected' : '' }}>
                        {{ $row }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label>Kategori Kegiatan</label>
            <select id="typeActivity" name="typeActivity" class="form-control select2" required>
                <option value="">-- Pilih --</option>
            </select>
        </div>
    </div>

    <div class="col-sm-2">
        <div class="form-group">
            <label>Tingkat</label>
            <select name="level" class="form-control select2">
                <option value="">-- Pilih --</option>
                @foreach ($options['optLevel'] as $index => $row)
                    <option value="{{ $index }}" 
                        {{ isset($data['level_id']) && $data['level_id'] == $index ? 'selected' : '' }}>
                        {{ $row }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label>Peran</label>
            <select name="role" class="form-control select2">
                <option value="">-- Pilih --</option>
                @foreach ($options['optRoleAct'] as $index => $row)
                    <option value="{{ $index }}" 
                        {{ isset($data['role_activity_id']) && $data['role_activity_id'] == $index ? 'selected' : '' }}>
                        {{ $row }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-sm-1">
        <div class="form-group">
            <label>Poin</label>
            <input name="point" type="number" class="form-control" step="any" placeholder="Poin" value="{{ isset($data['point']) ? $data['point'] : null }}" required>
        </div>
    </div>
</div>

@push('custom-script')
<script>
function onChangeParentAct(code) {
    $('#typeActivity').html('');
    $('#typeActivity').append('-- Pilih --');

    $.ajax({
        url: @json(route('typeActivity.dataOptKegiatan')),
        data: {
            code: code
        },
        method: 'POST'
    })
    .done(function(res) {
        let selectedId = @json( isset($data['activity_id']) ? $data['activity_id'] : '' );

        $.each(res, function( index, value ) {
            $('#typeActivity').append('<option value="' + index + '" ' + (selectedId == index ? 'selected' : '') + '>' + value + '</option>');
        });
    });
}    

$(function() {
    let valParentAct = $('#parentActivity').val();
    onChangeParentAct(valParentAct);

    $('#parentActivity').on('change', function() {
        let value = $('#parentActivity').val();
        onChangeParentAct(value);
    });
})
</script>
@endpush