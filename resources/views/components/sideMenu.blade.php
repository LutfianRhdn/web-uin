@push('custom-style')
<style>
  li.nav-item a {
    font-size: 14px !important;
  }
</style>
@endpush

@php
  $codeSuperUser = 'SU';
  $codeDosen = 'DSN';
  $codeMahasiswa = 'MHS';

  $roleIdLogin = Auth::user()->role->code;
@endphp

@if (in_array($roleIdLogin, [$codeSuperUser, $codeDosen, $codeMahasiswa]))
<li class="nav-item">
  <a href="{{ route('dashboard.index') }}"
    class="nav-link {{ (request()->is('/') || request()->is('dashboard')) ? 'active' : '' }}">
    <i class="nav-icon fas fa-bars"></i>
    <p>Dashboard</p>
  </a>
</li>
@endif

@if (in_array($roleIdLogin, [$codeSuperUser, $codeDosen, $codeMahasiswa]))
<li class="nav-item">
  <a href="{{ route('leaderboard.index') }}"
    class="nav-link {{ (request()->is('leaderboard')) ? 'active' : '' }}">
    <i class="nav-icon fas fa-medal"></i>
    <p>Leaderboard</p>
  </a>
</li>
@endif

@if (in_array($roleIdLogin, [$codeSuperUser, $codeMahasiswa]))
<li class="nav-item has-treeview menu-open">
  <a href="javascript:;" class="nav-link" onclick='onRedirectPage("{{ route("activity.indexMenu") }}")'>
    <i class="nav-icon fas fa-tachometer-alt"></i>
    <p>
      Kegiatan Mahasiswa
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>

  <ul class="nav nav-treeview ml-3">
    <li class="nav-item">
      <a href="{{ route('activity.create', 'keilmuan') }}" class="nav-link {{ (request()->is('activity/keilmuan') || request()->is('activity/keilmuan/*')) ? 'active' : '' }}">
        <i class="fas fa-book mr-2"></i>
        <p>Penalaran & Keilmuan</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('activity.create', 'kepemimpinan') }}" class="nav-link {{ (request()->is('activity/kepemimpinan') || request()->is('activity/kepemimpinan/*')) ? 'active' : '' }}">
        <i class="fas fa-users mr-2"></i>
        <p>Organisasi & Kepemimpinan</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('activity.create', 'minat-bakat') }}" class="nav-link {{ (request()->is('activity/minat-bakat') || request()->is('activity/minat-bakat/*')) ? 'active' : '' }}">
        <i class="fas fa-medal mr-2"></i>
        <p>Minat & Bakat</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('activity.create', 'pengabdian') }}" class="nav-link {{ (request()->is('activity/pengabdian') || request()->is('activity/pengabdian/*')) ? 'active' : '' }}">
        <i class="fas fa-chart-line mr-2"></i>
        <p>Pengabdian Pada Masyarakat</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('activity.create', 'kewirausahaan') }}" class="nav-link {{ (request()->is('activity/kewirausahaan') || request()->is('activity/kewirausahaan/*')) ? 'active' : '' }}">
        <i class="fas fa-university mr-2"></i>
        <p>Kewirausahaan</p>
      </a>
    </li>
  </ul>
</li>
@endif

@if (in_array($roleIdLogin, [$codeSuperUser, $codeDosen]))
<li class="nav-item has-treeview menu-open">
  <a href="javascript:;" class="nav-link">
    <i class="nav-icon fas fa-sign-in-alt"></i>
    <p>
      Portofolio Mahasiswa
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>

  <ul class="nav nav-treeview ml-3">
    <li class="nav-item">
      <a href="{{ route('portofolio.index', 'verify') }}" class="nav-link {{ (request()->is('portofolio/verify') || request()->is('portofolio/verify/*')) ? 'active' : '' }}">
        <i class="fas fa-check-circle mr-2"></i>
        <p>Terverifikasi</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('portofolio.index', 'waiting') }}" class="nav-link {{ (request()->is('portofolio/waiting') || request()->is('portofolio/waiting/*')) ? 'active' : '' }}">
        <i class="fas fa-question-circle mr-2"></i>
        <p>Menunggu Verifikasi</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('portofolio.index', 'reject') }}" class="nav-link {{ (request()->is('portofolio/reject') || request()->is('portofolio/reject/*')) ? 'active' : '' }}">
        <i class="fas fa-times mr-2"></i>
        <p>Ditolak</p>
      </a>
    </li>
  </ul>
</li>
@endif

@if (in_array($roleIdLogin, [$codeSuperUser, $codeDosen]))
<li class="nav-item">
  <a href="{{ route('colleger.index') }}"
    class="nav-link {{ (request()->is('colleger')) ? 'active' : '' }}">
    <i class="nav-icon fa fa-graduation-cap"></i>
    <p>Daftar Mahasiswa</p>
  </a>
</li>
@endif

@if (in_array($roleIdLogin, [$codeSuperUser]))
<li class="nav-item">
  <a href="{{ route('submissionLimit.indexReview') }}"
    class="nav-link {{ (request()->is('submission-limit/review')) ? 'active' : '' }}">
    <i class="nav-icon fas fa-search"></i>
    <p>Pengajuan Persetujuan Dosen</p>
  </a>
</li>
@endif

@if (in_array($roleIdLogin, [$codeSuperUser]))
<li class="nav-item has-treeview menu-open">
  <a href="javascript:;" class="nav-link">
    <i class="nav-icon fas fa-sign-in-alt"></i>
    <p>
      Master Data
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>

  <ul class="nav nav-treeview ml-3">
    <li class="nav-item">
      <a href="{{ route('user.index') }}" class="nav-link {{ (request()->is('master/user') || request()->is('master/user/*')) ? 'active' : '' }}">
        <i class="fas fa-users mr-2"></i>
        <p>Data Pengguna</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('mActivity.index') }}" class="nav-link {{ (request()->is('master/activity') || request()->is('master/activity/*')) ? 'active' : '' }}">
        <i class="fa fa-book mr-2"></i>
        <p>Data Aktivitas</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('mLevel.index') }}" class="nav-link {{ (request()->is('master/level') || request()->is('master/level/*')) ? 'active' : '' }}">
        <i class="fa fa-book mr-2"></i>
        <p>Data Tingkat Aktivitas</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('mRoleActivity.index') }}" class="nav-link {{ (request()->is('master/role-activity') || request()->is('master/role-activity/*')) ? 'active' : '' }}">
        <i class="fa fa-book mr-2"></i>
        <p>Data Peran Aktivitas</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('typeActivity.index') }}" class="nav-link {{ (request()->is('master/type-activity') || request()->is('master/type-activity/*')) ? 'active' : '' }}">
        <i class="fa fa-book mr-2"></i>
        <p>Data Tipe Aktivitas</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('semesterMission.index') }}" class="nav-link {{ (request()->is('master/semester-mission') || request()->is('master/semester-mission/*')) ? 'active' : '' }}">
        <i class="fa fa-book mr-2"></i>
        <p>Data Misi Per-Semester</p>
      </a>
    </li>
  </ul>
</li>
@endif

@if (in_array($roleIdLogin, [$codeSuperUser]))
<li class="nav-item">
  <a href="{{ route('setting.index') }}"
    class="nav-link {{ (request()->is('setting')) ? 'active' : '' }}">
    <i class="nav-icon fas fa-cog"></i>
    <p>Pengaturan</p>
  </a>
</li>
@endif

@if (in_array($roleIdLogin, [$codeDosen]))
<li class="nav-item">
  <a href="{{ route('submissionLimit.index') }}" class="nav-link {{ (request()->is('submission-limit')) ? 'active' : '' }}">
    <i class="nav-icon fas fa-paper-plane"></i>
    <p>Pengajuan Batas Persetujuan</p>
  </a>
</li>
@endif

@if (in_array($roleIdLogin, [$codeDosen]))
<li class="nav-item">
  <a href="{{ route('report.indexDosen') }}"
    class="nav-link {{ (request()->is('report/dosen')) ? 'active' : '' }}">
    <i class="nav-icon fas fa-book"></i>
    <p>Laporan Persetujuan Dosen</p>
  </a>
</li>
@endif

@if (in_array($roleIdLogin, [$codeMahasiswa]))
<li class="nav-item">
  <a href="{{ route('profile.indexSetting') }}"
    class="nav-link {{ (request()->is('profile/setting')) ? 'active' : '' }}">
    <i class="nav-icon fas fa-cog"></i>
    <p>Pengaturan Akun</p>
  </a>
</li>
@endif

<script>
function onRedirectPage(urlRedirect) {
  window.location = urlRedirect;
}
</script>