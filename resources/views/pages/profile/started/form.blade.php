@extends('layouts.master')

@push('custom-style')
<style>
  body {
    padding-top: 20px;
    background-color: #f8f9fa;
  }
</style>
@endpush

@section('body')
<body>
  <!-- Profile Image -->
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <h3>Lapor</h3>
        <hr>
        <h4>Minat & Bakat</h4>
      </div>
    </div>

    <div class="row mt-4">
      <div class="col-12 col-md-8 offset-md-2">
        <div class="card">
          <div class="card-body">

            <form action="{{ route('profile.storeStarted') }}" method="POST" enctype="multipart/form-data">
              @csrf

              <div class="row">
                <div class="col-12 text-center">
                  <img src="{{ asset('img/logo.svg') }}" height="50px" width="50px">
                  <br>
                  <b>Silahkan Lengkapi</b>
                  <hr>

                  @if (Session::get('success') != null)
                    <div class="alert alert-info alert-dismissible text-left">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h5><i class="icon fas fa-check"></i> Berhasil!</h5>
                      {{ Session::get('success') }}
                    </div>
                  @endif

                  @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible text-left">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h5><i class="icon fas fa-ban"></i> Terjadi Kesalahan!</h5>
                      <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>
                  @endif
                </div>
              </div>

              <div class="row" style="margin-bottom: 40px;">
                <div class="col-12 text-center">
                  <b>Silakan masukan prestasi anda selama SMA dengan menuliskan nama kegiatan dan mengupload bukti sertifikatnya, jika tidak ada silakan langsung klik submit.</b>
                </div>
              </div>

              <div class="row">
                <div class="col-12">

                  <div class="row">
                    <div class="col-12">
                      <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle"
                          src="{{ showPhotoProfile(Auth::user()->photo) }}"
                          alt="Photo Profile">
                      </div>

                      <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>
                      <p class="text-muted text-center">{{ Auth::user()->role->name }}</p>
                    </div>
    
                    <div class="col-6">
                      <div class="form-group">
                        <label>Angkatan</label>
                        <input name="generation" type="text" class="form-control" placeholder="Tahun Angkatan" value="{{ isset($data['generation']) ? $data['generation'] : null }}">
                      </div>
                    </div>
  
                    <div class="col-6">
                      <div class="form-group">
                        <label>No. Handphone</label>
                        <input name="phone" type="text" class="form-control" placeholder="08x xxx xxx xxx" value="{{ \Auth::user()->phone }}">
                      </div>
                    </div>
                  </div>

                  <hr>

                </div>

                <div class="col-12 pt-1 content-activity">
                  <div class="row wrap-form">
                    <div class="col-6">
                      <div class="form-group">
                        <label>Nama Kegiatan</label>
                        <input name="name_activity[]" type="text" class="form-control" placeholder="Nama Kegiatan">
                      </div>
                    </div>
  
                    <div class="col-5">
                      <div class="form-group">
                        <label>Upload Dokumen</label>
                        <div class="input-group">
                          <div class="custom-file">
                            <input name="file_document[]" type="file" class="custom-file-input" id="fileDocument">
                            <label class="custom-file-label" for="fileDocument">Pilih File</label>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-1 align-self-end mb-3">
                      <button type="button" class="btn btn-default btn-block remove-activity" disabled><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                </div>

                <div class="col-12">
                  <div class="row">
                    <div class="col-12">
                      <button type="button" class="btn btn-default btn-block" id="addActivity">Tambah Kegiatan</button>
                    </div>
                  </div>
                </div>

                <div class="col-12 col-md-4 offset-md-8 mt-3">
                  <div class="text-right">
                    <div class="row">
                      <div class="col-12 col-sm-6 mb-2">
                        <button class="btn btn-primary btn-block">Submit</button>
                      </div>
                      <div class="col-12 col-sm-6 mb-2">
                        <a href="{{ route('dashboard.index') }}" class="btn btn-danger btn-block">Exit</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</body>
@endsection

@push('custom-script')
<script>

  // Init Custom File Input
  bsCustomFileInput.init();

  // Get html content activity
  let htmlContentAct = $('.content-activity').html();

  // Event click button add activity
  $('#addActivity').on('click', function() {
    $('.content-activity').append(htmlContentAct);
    bsCustomFileInput.init();

    if (countContentActivity() > 1) {
      $('.remove-activity').attr("disabled", false);
    }
  });

  // Event click button remove activity
  $(document).on('click', '.remove-activity', function() {
    Swal.fire({
      title: 'Konfirmasi',
      text: "Yakin ingin menghapus kegiatan ini?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya!',
      cancelButtonText: 'Tidak!'
    }).then((result) => {
      if (result.value == true) {
        let index = $('.remove-activity').index(this);
        $('.wrap-form').eq(index).remove();

        if (countContentActivity() < 2) {
          $('.remove-activity').attr("disabled", true);
        }
      }
    });
  });

  // Function count content activity
  function countContentActivity() {
    return $('.content-activity .wrap-form').length;
  }

</script>
@endpush