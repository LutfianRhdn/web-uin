@extends('layouts.panel')

@section('title-page') {{ $titlePage }} @endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">{{ $titlePage }}</h3>
      </div>

      <div class="card-body">
        <div class="row">
          <div class="col-12 col-md-3 mb-3">
            <div class="text-center">
              <img class="profile-user-img img-fluid img-circle"
                src="{{ showPhotoProfile($data->profileUser->user->photo) }}"
                style="width: 100px; height: 100px;"
                alt="Photo Profile">
            </div>
            <h3 class="profile-username text-center">{{ $data->profileUser->user->name }}</h3>
            <p class="text-muted text-center">{{ $data->profileUser->user->role->name }}</p>
            <ul class="list-group list-group-unbordered mb-3">
              <li class="list-group-item">
                <span class="float-left mr-2">
                  <i class="fas fa-phone"></i>
                </span>
                <span class="float-left">{{ $data->profileUser->user->phone != null ? $data->profileUser->user->phone : '-' }}</span>
              </li>
              <li class="list-group-item">
                <span class="float-left mr-2">
                  <i class="fas fa-address-book"></i>
                </span>
                <span class="float-left">{{ $data->profileUser->user->address != null ? $data->profileUser->user->address : '-' }}</span>
              </li>
            </ul>
          </div>
          <div class="col-12 col-md-4 mb-3">
            <h5 class="text-muted">{{ $data->typeActivity->activity->name }}</h5>

            <h3>{{ $data->name }}</h3>

            <div class="text-muted" style="font-size: 18px;">
              <div class="row">
                <div class="col-3">Tingkat</div>
                <div class="col-9">: {{ $data->typeActivity->level != null ? $data->typeActivity->level->name : '-' }}</div>
              </div>
              <div class="row">
                <div class="col-3">Peran</div>
                <div class="col-9">: {{ $data->typeActivity->roleActivity != null ? $data->typeActivity->roleActivity->name : '-' }}</div>
              </div>
              <div class="row">
                <div class="col-3">Tanggal</div>
                <div class="col-9">: {{ formatDate($data->implementation_date) }}</div>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-3 mb-3">

            <h4>Lampiran</h4>

            @if (checkFormatImg($data->file_document))
              <img class="img-thumbnail" src="{{ $data->file_document != null ? asset('uploads/file_document/' . $data->file_document) : asset('img/' . $data->file_document) }}" style="width: 100%">
            @else
              <ul>
                <li><a href="{{ asset('uploads/file_document/' . $data->file_document) }}">{{ $data->file_document }}</a></li>
              </ul>
            @endif

          </div>
        </div>
      </div>

      <div class="card-footer text-right">
        @if ($data->status == 'WAITING')
          <button type="button" class="btn btn-danger btn-act-reject">Tolak</button>
          <button type="button" class="btn btn-success btn-act-verify">Setujui</button>
        @endif
      </div>
    </div>
  </div>
</div>
<form id="formApprove" action="{{ route('portofolio.postApproveData', $data->id) }}" method="POST"> @csrf </form>
<form id="formReject" action="{{ route('portofolio.postRejectData', $data->id) }}" method="POST"> @csrf </form>
@endsection

@push('custom-script')
<script>
$('.btn-act-reject').click(function() {
  Swal.fire({
    title: "Yakin ingin menolak data ini?",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Tidak'
  })
  .then((result) => {
    if (result.value == true) {
      $('#formReject').submit();
    }
  });
})

$('.btn-act-verify').click(function() {
  Swal.fire({
    title: "Yakin ingin menyetujui data ini?",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Tidak'
  })
  .then((result) => {
    if (result.value == true) {
      $('#formApprove').submit();
    }
  });
})
</script>
@endpush