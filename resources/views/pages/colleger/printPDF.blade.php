<html>
<head>
  <title>Laporan Profil Mahasiswa {{ ucwords($data->name) }}</title>
  <style>
    .text-left {
      text-align: left;
    }

    .text-center {
      text-align: center;
    }

    .text-uppercase {
      text-transform: uppercase;
    }

    .img-circle {
      border-radius: 100%;
    }
  </style>
</head>
<body>
  <h3 class="text-center text-uppercase m-0 p-0">Laporan Profil Mahasiswa {{ ucwords($data->name) }}</h3>

  <table border="1" cellspacing="0" cellpadding="7" style="width: 100%">
    <tbody>
      
      <tr>
        <th colspan="2">Biodata</th>
      </tr>
      <tr>
        <th class="text-left" style="width: 100px">Nama</th>
        <td>{{ ucwords($data->name) }}</td>
      </tr>
      <tr>
        <th class="text-left" style="width: 100px">Peran</th>
        <td>{{ isset($data->role) ? $data->role->name : '-' }}</td>
      </tr>
      <tr>
        <th class="text-left" style="width: 100px">No. Telp/HP</th>
        <td>{{ $data->phone != null ? $data->phone : '-' }}</td>
      </tr>
      <tr>
        <th class="text-left" style="width: 100px">Alamat</th>
        <td>{{ $data->address != null ? $data->address : '-' }}</td>
      </tr>
    </tbody>
  </table>

  <br>

  <table border="1" cellspacing="0" cellpadding="10" style="width: 100%">
    <thead>
      <tr>
        <th class="text-uppercase" colspan="3">Skor</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          Total Point <br>
          <b>{{ $data->profile->profile_score }} Point</b>
        </td>
        <td>
          Organisasi <br>
          <b>{{ $collectPoint['organisasi'] }} Point</b>
        </td>
        <td>
          Minat & Bakat <br>
          <b>{{ $collectPoint['minat_bakat'] }} Point</b>
        </td>
      </tr>
      <tr>
        <td>
          Pengabdian <br>
          <b>{{ $collectPoint['pengabdian'] }} Point</b>
        </td>
        <td>
          Kewirausahaan <br>
          <b>{{ $collectPoint['kewirausahaan'] }} Point</b>
        </td>
        <td>
          Keilmuan <br>
          <b>{{ $collectPoint['keilmuan'] }} Point</b>
        </td>
      </tr>
    </tbody>
  </table>

  <br>

  <table border="1" cellspacing="0" cellpadding="10" style="width: 100%">
    <thead>
      <tr>
        <th class="text-uppercase">Prestasi Mahasiswa</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          @if (count($data->activity) > 0)
            <ol style="padding: 0px; margin: 0px; margin-left: 20px;">
            @foreach ($data->activity as $row)
              <li>{{ $row->name != null ? $row->name : $row->file_document }}</li>
            @endforeach
            </ol>
          @else
            Tidak ada data prestasi
          @endif
        </td>
      </tr>
    </tbody>
  </table>

  <br>

  <table border="1" cellspacing="0" cellpadding="8" style="width: 100%">
    <thead>
      <tr>
        <th style="width: 50px;">No</th>
        <th>Nama Kegiatan</th>
        <th>Kategori</th>
        <th>Tingkat</th>
        <th>Peran</th>
        <th>Skor</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      @if (count($portofolio) > 0)
        @php $countIndex = 0 @endphp
        @foreach ($portofolio as $row)
          <tr>
            <td class="text-center">{{ $countIndex + 1 }}</td>
            <td class="text-left">{{ $row->name }}</td>
            <td class="text-left">{{ isset($row->typeActivity->activity) ? $row->typeActivity->activity->name : '-' }}</td>
            <td class="text-center">{{ isset($row->typeActivity->level) ? $row->typeActivity->level->name : '-' }}</td>
            <td class="text-center">{{ isset($row->typeActivity->roleActivity->name) ? $row->typeActivity->roleActivity->name : '-' }}</td>
            <td class="text-center">{{ $row->total_score }}</td>
            <td class="text-center">
              @php
                $value = '-';
                switch ($row->status) {
                  case 'WAITING':
                    $value = 'Menunggu Verifikasi';
                    break;
                  case 'VERIFY':
                    $value = 'Terverifikasi';
                    break;
                  case 'REJECT':
                    $value = 'Ditolak';
                    break;
                }

                echo $value;
              @endphp
            </td>
          </tr>
          @php $countIndex++ @endphp
        @endforeach
      @else
        <tr>
          <td class="text-center" colspan="7">Tidak ada portofolio</td>
        </tr>
      @endif
    </tbody>
  </table>

</body>
</html>