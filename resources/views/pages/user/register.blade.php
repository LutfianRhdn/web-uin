@extends('layouts.auth')

@section('content')

<div class="login-logo">
  <a href="#"><b>Daftar</b></a>
</div>

@if ($errors->any())
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h5><i class="icon fas fa-ban"></i> Terjadi Kesalahan!</h5>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

<div class="card">
  <div class="card-body login-card-body">
    <p class="login-box-msg">Daftar untuk mengakses aplikasi ini</p>

    <form action="{{ route('auth.storeRegister') }}" method="POST"  enctype="multipart/form-data" autocomplete="off">
      @csrf

      <div class="input-group mb-3">
        <input type="text" name="no_id" class="form-control" placeholder="NIM" autocomplete="off" value="{{ old('no_id') }}" required>
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-id-card"></span>
          </div>
        </div>
      </div>

      <div class="input-group mb-3">
        <input type="text" name="name" class="form-control" placeholder="Nama Lengkap" value="{{ old('name') }}" required>
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-user"></span>
          </div>
        </div>
      </div>

      <div class="input-group mb-3">
        <input type="email" name="email" class="form-control" placeholder="Email"  value="{{ old('email') }}" required>
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-envelope"></span>
          </div>
        </div>
      </div>

      <div class="input-group mb-3">
        <input type="password" name="password" class="form-control input-password" placeholder="Password" required>
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-lock"></span>
          </div>
        </div>
      </div>

      <label class="mb-3" style="font-weight: normal">
        <input type="checkbox" class="show-input-password mr-1">
        Lihat Sandi
      </label>

      <div class="input-group mb-3">
        <input type="password" name="password_confirmation" class="form-control input-password-confirm" placeholder="Konfirmasi Password" required>
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-lock"></span>
          </div>
        </div>
      </div>

      <div class="input-group mb-3">
        <input type="text" name="phone" class="form-control" placeholder="No. HP/Telepon" value="{{ old('phone') }}">
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-phone"></span>
          </div>
        </div>
      </div>

      <div class="input-group mb-3">
        <textarea name="address" class="form-control" rows="3" placeholder="Alamat">{{ old('address') }}</textarea>
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-address-book"></span>
          </div>
        </div>
      </div>

      <div class="form-group mb-3">
        <select name="semester" class="form-control" required>
          <option value="">-- Pilih Semester --</option>
          @foreach ($semester as $index => $row)
            <option value="{{ $index }}" {{ $index == old('semester') ? 'selected' : '' }}>{{ $row }}</option>
          @endforeach
        </select>
      </div>

      <div class="row">
        <div class="col-12">
          <button dusk="submit-button" type="submit" class="btn btn-danger btn-block">Daftar</button>
        </div>
      </div>
    </form>

    <div class="social-auth-links text-center mb-3">
      <p>- OR -</p>
      <a href="{{ route('auth.indexLogin') }}" class="btn btn-block btn-primary">
        Masuk
      </a>
    </div>
  </div>
</div>
@endsection