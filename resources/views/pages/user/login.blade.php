@extends('layouts.auth')

@section('content')

  <div class="login-logo">
    <a href="{{ url('') }}"><b>Lapor</b></a>
  </div>

  @if (Session::get('success') != null)
    <div class="alert alert-info alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h5><i class="icon fas fa-check"></i> Berhasil!</h5>
      {{ Session::get('success') }}
    </div>
  @endif

  @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h5><i class="icon fas fa-ban"></i> Terjadi Kesalahan!</h5>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
  
  <div class="card">
    <div class="card-body login-card-body">

      <div class="px-5">
        <img class="img-fluid" src="{{ asset('img/logo.svg') }}">
      </div>

      <p class="login-box-msg">Masuk untuk memulai sesimu</p>

      <form action="{{ route('auth.storeLogin') }}" method="POST">
        @csrf

        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control" placeholder="Email" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control input-password" placeholder="Password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        
        <div class="row">
          <div class="col-6">
            <div class="icheck-primary">
              <label for="remember">
                <input type="checkbox" name="remember" id="remember" class="mr-1">  
                Ingat Aku
              </label>
            </div>
          </div>
          <div class="col-6 text-right">
            <label class="mb-3" style="font-weight: normal">
              <input type="checkbox" class="show-input-password mr-1">
              Lihat Sandi
            </label>
          </div>
        </div>

        <button type="submit" class="btn btn-primary btn-block">Masuk</button>

      </form>

      <div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <a href="{{ route('auth.indexRegister') }}" class="btn btn-block btn-danger">
          Daftar
        </a>
      </div>
    </div>
    <!-- /.login-card-body -->
  </div>

@endsection