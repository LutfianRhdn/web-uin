@extends('layouts.panel')

@push('custom-style')
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
.info-box-icon {
  padding: 20px 0px;
}

.info-box-content {
  padding: 0px !important;
  padding-top: 5px !important;
  display: inline-block;
}

@media only screen and (min-width: 600px) {
  .info-box-content {
    padding: 10px !important;
    display: inline-block;
  }
}
</style>
@endpush

@section('title-page') Dashboard @endsection
@section('content')

  @if (\Auth::user()->role_id == '3')
    <!-- Info boxes -->
    <div class="row">

      <div class="col-4">
        <div class="info-box">
          <div class="row no-gutters w-100">
            <div class="col-md-2">
              <span class="info-box-icon bg-primary elevation-1 h-100 w-100">
                <i class="fas fa-arrow-up"></i>
              </span>
            </div>
            <div class="col-md-6">
              <div class="info-box-content">
                <span class="info-box-text">Total Point</span>
                <span class="info-box-number">{{ \Auth::user()->profile->profile_score }} Point</span>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-4">
        <div class="info-box">
          <div class="row no-gutters w-100">
            <div class="col-md-2">
              <span class="info-box-icon bg-success elevation-1 h-100 w-100">
                <i class="fas fa-users"></i>
              </span>
            </div>
            <div class="col-md-6">
              <div class="info-box-content">
                <span class="info-box-text">Organisasi</span>
                <span class="info-box-number">{{ $collectPoint['ORGANISASI_KEPEMIMPINAN'] }} Point</span>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-4">
        <div class="info-box">
          <div class="row no-gutters w-100">
            <div class="col-md-2">
              <span class="info-box-icon bg-warning elevation-1 h-100 w-100">
                <i class="fas fa-medal fa-icon-white"></i>
              </span>
            </div>
            <div class="col-md-6">
              <div class="info-box-content">
                <span class="info-box-text">Minat & Bakat</span>
                <span class="info-box-number">{{ $collectPoint['MINAT_BAKAT'] }} Point</span>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-4">
        <div class="info-box">
          <div class="row no-gutters w-100">
            <div class="col-md-2">
              <span class="info-box-icon bg-danger elevation-1 h-100 w-100">
                <i class="fas fa-chart-line"></i>
              </span>
            </div>
            <div class="col-md-6">
              <div class="info-box-content">
                <span class="info-box-text">Pengabdian</span>
                <span class="info-box-number">{{ $collectPoint['PENGABDIAN_MASYARAKAT'] }} Point</span>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-4">
        <div class="info-box">
          <div class="row no-gutters w-100">
            <div class="col-md-2">
              <span class="info-box-icon bg-info elevation-1 h-100 w-100">
                <i class="fas fa-university"></i>
              </span>
            </div>
            <div class="col-md-6">
              <div class="info-box-content">
                <span class="info-box-text">Kewirausahaan</span>
                <span class="info-box-number">{{ $collectPoint['KEWIRAUSAHAAN'] }} Point</span>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-4">
        <div class="info-box">
          <div class="row no-gutters w-100">
            <div class="col-md-2">
              <span class="info-box-icon bg-info elevation-1 h-100 w-100">
                <i class="fas fa-book"></i>
              </span>
            </div>
            <div class="col-md-6">
              <div class="info-box-content">
                <span class="info-box-text">Keilmuan</span>
                <span class="info-box-number">{{ $collectPoint['PENALARAN_KEILMUAN'] }} Point</span>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  @endif

  <!-- Profile Image -->
  <div class="row">
    <div class="col-12 col-sm-6 col-md-3">
      <div class="card card-primary card-outline">
        <div class="card-body box-profile">
          <div class="text-center">
            <img class="profile-user-img img-fluid img-circle"
              src="{{ showPhotoProfile(Auth::user()->photo) }}"
              alt="Photo Profile"
              style="width: 100px; height: 100px;">
          </div>

          <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>
          <p class="text-muted text-center">{{ Auth::user()->role->name }}</p>

          <ul class="list-group list-group-unbordered mb-3">
            <li class="list-group-item">
              <span class="float-left mr-2">
                <i class="fas fa-phone"></i>
              </span>
              <span class="float-left">{{ Auth::user()->phone != null ? Auth::user()->phone : '-' }}</span>
            </li>
            <li class="list-group-item">
              <span class="float-left mr-2">
                <i class="fas fa-address-book"></i>
              </span>
              <span class="float-left">{{ Auth::user()->address != null ? Auth::user()->address : '-' }}</span>
            </li>

            <a href="{{ route('auth.storeLogout') }}" class="btn btn-primary btn-block mt-3"><b>Keluar</b></a>
          </ul>
        </div>
      </div>
    </div>

    @if (Auth::user()->role->code == 'MHS')
    {{-- Semester --}}
    <div class="col-12 col-sm-6 col-md-3">
      <div class="card card-primary card-outline">
        <div class="card-body box-profile">
          <h3 class="profile-username">Misi Angkatan {{ date('Y') }}</h3>

          @if (isset($missionDone) && count($missionDone) > 0)
          <ul class="list-group list-group-unbordered mt-3">
            @foreach ($missionDone as $row)
            <li class="list-group-item">
              <span class="float-left text-bold">{{ $row->parentActivity != null ? $row->parentActivity->name : '-' }}</span>
              <span class="float-right">{{ $row->current_score }}/{{ $row->target_total_score }}</span>
            </li>
            @endforeach
          </ul>
          @else
          <ul class="list-group list-group-unbordered mt-3">
            <li class="list-group-item">
              <span class="float-left">- Tidak ada misi -</span>
            </li>
          </ul>
          @endif
        </div>
      </div>
    </div>

    {{-- Prestasi --}}
    <div class="col-12 col-sm-12 col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Prestasi</h3>
        </div>
        <div class="card-body table-responsive" style="height: 340px;">
          <div class="table-responsive">
            <table class="table table-head-fixed text-nowrap data-prestasi">
              <thead>
                <tr>
                  <th style="width: 50px;">No</th>
                  <th>Nama</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    @endif

  </div>

  @if (Auth::user()->role->code == 'MHS')
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Portofolio Kegiatan</h3>
        </div>
        <div class="card-body table-responsive" style="max-height: 500px;">
          <div class="table-responsive">
            <table class="table table-head-fixed text-nowrap data-portofolio">
              <thead>
                <tr>
                  <th style="width: 50px;">No</th>
                  <th>Tipe Kegiatan</th>
                  <th>Kategori Kegiatan</th>
                  <th>Tingkat</th>
                  <th>Peran</th>
                  <th>Nama Kegiatan</th>
                  <th>Tanggal Pelaksanaan</th>
                  <th>File Dokumen</th>
                  <th>Status</th>
                  <th>Tanggal Upload</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif

@endsection

@if (Auth::user()->role->code == 'MHS')
  @push('custom-script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script type="text/javascript">
    $(function () {
      let dataColumnsPrestasi = @json($dataColumnsPrestasi);
      $('.data-prestasi').DataTable({
          responsive: true,
          serverSide: true,
          ajax: "{{ $urlDataPrestasi }}",
          columns: dataColumnsPrestasi
      });

      let dataColumnsPortofolio = @json($dataColumnsPortofolio);
      $('.data-portofolio').DataTable({
          responsive: true,
          serverSide: true,
          ajax: "{{ $urlDataPortofolio }}",
          columns: dataColumnsPortofolio
      });
    });
  </script>
  @endpush
@endif